{{Short description|Designation of part of the Saturn V rocket}}
{{Use American English|date=January 2020}}
{{Use dmy dates|date=January 2020}}
{{multiple image
| align = right
| image1 = J002e3 animated.gif
| width1 = {{#expr: (200* (351/ 344)) round 0}}
| alt1 = 
| caption1 = J002E3 discovery images taken by Bill Yeung on September 3, 2002. J002E3 is in the circle.
| image2 = J002e3f orbit.gif
| width2 = {{#expr: (200 * (640/ 480)) round 0}}
| alt2 = 
| caption2 = Computer simulation of J002E3's motion, alternating between six Earth orbits and a heliocentric orbit (click on image to view animation)
| footer = 
}}
[[Image:AS17-148-22714 crop.jpg|thumb|200px|S-IVB stage of [[Apollo 17]]. The one used for Apollo 12 is of identical type.]]

'''J002E3''' is an object in space which is thought to be the [[S-IVB]] third [[Multistage rocket|stage]] of the [[Apollo 12]] [[Saturn V]] rocket. It was discovered on September 3, 2002, by amateur astronomer [[William Kwong Yu Yeung|Bill Yeung]]. Initially thought to be an [[asteroid]], it has since been tentatively identified as the third stage of Apollo 12 Saturn V based on [[spectroscopy|spectrographic]] evidence consistent with the [[titanium dioxide]] in the paint used on the rockets.<ref name="nasa_20021011_j002e3">{{cite news | url = https://spaceref.com/press-release/j002e3-an-update/ | title = J002E3: An Update | website = SpaceRef | publisher = [[NASA]] | date = 11 October 2002 | access-date = 25 August 2023 | first1 = Paul | last1 = Chodas | first2 = Steve | last2 = Chesley }}</ref><ref name=Jorgensen>{{cite journal | bibcode = 2003DPS....35.3602J| last1 = Jorgensen | first1 = K. | last2 = Rivkin | first2 = A. | last3 = Binzel | first3 = R. | last4 = Whitely | first4 = R. | last5 = Hergenrother | first5 = C. | last6 = Chodas | first6 = P. | last7 = Chesley | first7 = S. | last8 = Vilas | first8 = F. | title = Observations of J002E3: Possible Discovery of an Apollo Rocket Body | journal = [[Bulletin of the American Astronomical Society]] | date = May 2003 | volume = 35 | page = 981 | quote = Through the modeling of common spacecraft materials, the observations of J002E3 show a strong correlation of absorption features to a combination of human-made materials including white paint, black paint, and aluminum. Absorption features in the near IR show a strong correlation with paint containing a titanium-oxide semiconductor. Using the material model and the orbital information, it was concluded that J002E3 is a human-made object from an Apollo rocket upperstage, most likely Apollo 12. }}</ref><ref name="nasa_20020920_j002e3">{{cite news |title = First Confirmed Capture into Earth Orbit Is Likely Apollo Rocket |date = 20 September 2002 |url = https://www.jpl.nasa.gov/news/first-confirmed-capture-into-earth-orbit-is-likely-apollo-rocket |publisher = NASA |website = Jet Propulsion Laboratory |access-date = 25 August 2023 |archive-url = https://web.archive.org/web/20230825100344/https://www.jpl.nasa.gov/news/first-confirmed-capture-into-earth-orbit-is-likely-apollo-rocket |archive-date = 25 August 2023 |url-status = live}}</ref> The stage was intended to be injected into a permanent [[heliocentric orbit]] in November 1969, but is now believed instead to have gone into an unstable high Earth orbit which left Earth's proximity in 1971 and again in June 2003, with an approximately 40-year cycle between heliocentric and [[geocentric orbit]].<ref name="nasa_20020911_j002e3">{{cite news |last = Chodas |first = Paul |date = 11 September 2002 |title = Newly Discovered Object Could be a Leftover Apollo Rocket Stage |url = https://cneos.jpl.nasa.gov/news/news134.html |publisher = NASA |website = Center for Near Eath Object Studies |access-date = 25 August 2023 |archive-url = https://web.archive.org/web/20180903154737/https://cneos.jpl.nasa.gov/news/news134.html |archive-date = 3 September 2018 |url-status = live}}</ref>

==Discovery==
When it was first discovered, it was quickly found that the object was in an [[orbit]] around [[Earth]]. Astronomers were surprised at this, as the [[Moon]] is the only large object in orbit around the Earth,<ref group=lower-alpha>Also orbiting the Earth are the [[Kordylewski cloud]]s: large transient concentrations of dust at the [[Trojan points]] of the Earth–Moon system, discovered in 1956 by the [[Poland|Polish]] astronomer [[Kazimierz Kordylewski]].</ref> and anything else would have been ejected long ago due to [[perturbation (astronomy)|perturbation]]s with the Earth, the Moon and the [[Sun]].

Therefore, it probably entered into [[Geocentric orbit|Earth orbit]] very recently, yet there was no recently launched spacecraft that matched the orbit of J002E3. One explanation could have been that it was a 30&nbsp;meter-wide piece of rock, but [[University of Arizona]] astronomers found that spectral observations of the object indicated a strong correlation of absorption features with a combination of human-made materials including white paint, black paint, and aluminum, consistent with [[Saturn V]] rockets.<ref name=Jorgensen/> Back-tracing its orbit showed that the object had been orbiting the Sun for 31&nbsp;years and had last been in the vicinity of the Earth in 1971. This seemed to suggest that it was a part of the [[Apollo 14]] mission, but NASA knew the whereabouts of all hardware used for that mission; the third stage, for instance, was deliberately crashed into the Moon for seismic studies.

The most likely explanation appears to be the [[S-IVB]] third stage for [[Apollo 12]].<ref name="nasa_20021011_j002e3" /><ref name=Jorgensen/> Photometric observations of J002E3 made in February 2003 from the [[Air Force Maui Optical and Supercomputing observatory|Air Force Maui Optical and Supercomputing Site]] (AMOS) matched an S-IVB [[light curve]] model consisting of a diffuse cylinder tumbling with a period of 63.46 seconds and a precession of 79 ± 10°.<ref name="ieee_2004_j002e3model">{{cite conference |last1 = Lambert |first1 = John V. |last2 = Hamada |first2 = Kris |last3 = Hall |first3 = Doyle T. |last4 = Africano |first4 = John L. |last5 = Giffin |first5 = Maile |last6 = Luu |first6 = Kim Luu |last7 = Kervin |first7 = Paul |last8 = Jorgensen |first8 = Kira |year = 2004 |title = Photometric and spectral analysis of MPC object J002E3 |url = https://ieeexplore.ieee.org/document/1368093 |url-access = subscription |access-date = 25 August 2023 |publisher = IEEE |conference = IEEE Aerospace Conference |location = Big Sky, Montana |doi = 10.1109/AERO.2004.1368093 |oclc = 4801025397 |pages = 2866-2873 |quote = … the rotational period was determined to be 63.46 seconds … The value determined for the precession angle, 79 ± 10°, indicates that the object is in a near end-over-end tumble … The optical cross-sections for the cylinder and the ends, 14 ± 2 and 5 ± 2 square meters, respectively, are consistent with the size of the S-IVB and the expected darkening of the white paint due to space weathering. Comparing the known physical dimensions of the S-IVB to the determined optical cross-sections yields an albedo for the cylinder of 0.16 ± 0.02 (neglecting the black painted areas) and 0.22 ± 0.07 for the ends. |language = en }}</ref> NASA had originally planned to direct the S-IVB into a solar orbit, but an extra long burn of the [[ullage motor]]s meant that venting the remaining [[propellant]] in the tank of the S-IVB did not give the rocket stage enough energy to escape the Earth–Moon system, and instead the stage ended up in a semi-stable orbit around the Earth after passing by the Moon on 18&nbsp;November 1969.

It is thought that J002E3 left Earth orbit in June&nbsp;2003, and that it may return to orbit the Earth in the mid-2040s.<ref name="nasa_20021011_j002e3" />

==Potential impact with Earth or Moon==
The object's Earth orbital paths occasionally take it within the radius of the [[Moon]]'s orbit, and could result in eventual entry into Earth's atmosphere, or collision with the Moon. The Apollo&nbsp;12 empty S-IVB, Instrument Unit, and spacecraft adapter base, had a mass of about {{convert|30000|lb|t ST|disp=flip|sigfig=2}}.<ref name="Orloff">{{cite book |first=Richard W. |last=Orloff |title=Apollo by the Numbers: A Statistical Reference |url=https://history.nasa.gov/SP-4029/Apollo_18-19_Ground_Ignition_Weights.htm |date=September 2004 |chapter=Ground Ignition Weights}}</ref> This is less than one-fifth of the {{convert|169900|lb|t ST|adj=on|disp=flip}} mass of the [[Skylab|Skylab space station]], which was constructed from a similar S-IVB and fell out of orbit on 11&nbsp;July 1979.<ref name="skylaborbitalmass">{{cite web |url=http://www.aerospaceguide.net/spacestation/skylab.html|title=Skylab Space Station|date=6 June 2016 }}</ref> Objects with a mass of about {{convert|10|t|lb ST}} enter Earth's atmosphere approximately 10&nbsp;times a year,<ref>{{cite journal |last=Bland |first=Philip |date=December 2005 |title=The impact rate on Earth |journal=[[Philosophical Transactions of the Royal Society of London A]]  |volume=363 |pages=2793–2810 |bibcode=2005RSPTA.363.2793B |doi=10.1098/rsta.2005.1674 |issue=1837|pmid=16286291 |s2cid=19899735 }}</ref> one of which impacts the Earth's surface approximately once every 10&nbsp;years.<ref>{{cite journal |last1=Bland |first1=Philip A. |last2=Artemieva |first2=Natalya A. |title=The rate of small impacts on Earth |date=April 2006 |journal=[[Meteoritics]] |volume=41 |issue=4 |pages=607–631 |bibcode=2006M&PS...41..607B |doi=10.1111/j.1945-5100.2006.tb00485.x|s2cid=54627116 |doi-access=free }}</ref>

Ten essentially similar empty S-IVB stages from Apollo, Skylab and [[Apollo-Soyuz Test Project]] missions<ref group=lower-alpha>The Apollo missions were: [[AS-201]], [[AS-202]], [[Apollo 4]], [[Apollo 5]], [[Apollo 6]], and [[Apollo 7]]. The Skylab missions were [[Skylab 2]], [[Skylab 3]], and [[Skylab 4]].</ref> have re-entered the atmosphere from 1966 to 1975. In all cases (including the Skylab station), the objects burned in the atmosphere and broke into relatively small pieces, rather than striking the Earth as a single mass. On the other hand, these objects entered from low Earth orbit or a ballistic trajectory, with less energy than J002E3 might possibly have if it were to enter from solar orbit.

==See also==
*[[6Q0B44E]], space debris originally thought to be a meteoroid
*[[2006 RH120|2006 RH<sub>120</sub>]], a meteoroid originally thought to be space debris
*[[2007 VN84|2007 VN<sub>84</sub>]], an asteroid designation mistakenly given to the [[Rosetta (spacecraft)|''Rosetta'' spacecraft]]
*[[3753 Cruithne]]
*[[2020 SO]]
*[[Space debris]]

==Notes==
{{notelist|1}}

==References==
{{reflist}}

== External links ==
* [https://cneos.jpl.nasa.gov/doc/j002e3/ J002E3 Animations], CNEOS
* [https://web.archive.org/web/20091014042259/http://science.nasa.gov/headlines/y2002/images/mysteryobject/gallery/gallery_j002e3.html Mystery Object J002E3 Gallery], NASA
* [https://web.archive.org/web/20100324104952/http://science.nasa.gov/headlines/y2002/20sep_mysteryobject.htm Mystery Object Orbits Earth], NASA
* {{webarchive |url=https://web.archive.org/web/20070820231431/http://ali.opi.arizona.edu/cgi-bin/WebObjects/UANews.woa/2/wa/SRStoryDetails?ArticleID=6102UA |date=20 August 2007 |title=Astronomers Discover That Earth's Second Moon Wears Apollo Paint}}, University of Arizona

{{Apollo program}}

{{DEFAULTSORT:J002e3}}
[[Category:Apollo program]]
[[Category:Apollo 12]]
[[Category:Space debris]]
[[Category:Derelict satellites in heliocentric orbit]]
[[Category:Near-Earth objects in 2002]]
[[Category:Claimed moons of Earth]]
[[Category:Astronomical objects discovered in 2002|20020903]]
