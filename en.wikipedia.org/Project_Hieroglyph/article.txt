{{Infobox website
| name             = Hieroglyph
| logo             = 
| screenshot       =
| caption          =
| url              = [http://hieroglyph.asu.edu/ hieroglyph.asu.edu]
| alexa            =
| commercial       = No
| type             = 
| language         = [[English language|English]]
| registration     =
| owner            = [[Arizona State University]]
| author           =  [[Neal Stephenson]], Founder; Ed Finn, Editor; [[Kathryn Cramer]], Editor
| launch_date      = 2012
| current_status   =
| revenue          =
}}
'''Project Hieroglyph''' is an initiative to create [[science fiction]] that will spur innovation in science and technology founded by [[Neal Stephenson]] in 2011.<ref name="smag_201204_pessimistic" />

Stephenson framed the ideas behind Hieroglyph in a World Policy Institute article entitled "Innovation Starvation" <ref name="wpi_20110927_stephenson" /> where he attempts to rally writers to infuse science fiction with [[optimism]] that could inspire a new generation to, as he puts it, “get big stuff done.”

Stephenson says that "a good SF universe has a coherence and internal logic that makes sense to scientists and engineers. Examples include [[Isaac Asimov]]'s robots, [[Robert Heinlein]]'s rocket ships, and [[William Gibson]]'s [[cyberspace]]. Such icons serve as hieroglyphs—simple, recognizable symbols on whose significance everyone agrees."<ref name="asu_2012_hieroglyphhomepage" />

Stephenson partnered with [[Arizona State University]]'s Center for Science and the Imagination<ref name="asu_2019_csihomepage" /> which now administers the project.

In September 2014, the project's first book, Hieroglyph: Stories and Visions for a Better Future, edited by Ed Finn and [[Kathryn Cramer]] was published by [[William Morrow (publisher)|William Morrow]].<ref name="hieroglyph_2015_thebook" /> Contributors to the book include Neal Stephenson, [[Bruce Sterling]], [[Madeline Ashby]], [[Gregory Benford]], [[Rudy Rucker]], [[Vandana Singh]], [[Cory Doctorow]], [[Elizabeth Bear]], [[Karl Schroeder]], [[James Cambias]], [[Brenda Cooper]], [[Charlie Jane Anders]], [[Kathleen Ann Goonan]], [[Lee Konstantinou]], [[Annalee Newitz]], [[Geoffrey Landis]], [[David Brin]], [[Lawrence Krauss]], and [[Paul Davies]].

==See also==
*[[Collaborative innovation network]]
*[[Exploratory engineering]]
*[[Fictional technology]]
*[[Invention]]
*[[Macro-engineering]]
*[[Megaproject]]
*[[Megascale engineering]]
*[[The Mongoliad]]
*[[Retrofuturism]]
*[[Techno-progressivism]]
*[[Technological utopianism]]

==References==
{{reflist|refs=

<ref name="smag_201204_pessimistic">{{cite web
| last          = Newitz
| first         = Annalee
| url           = http://www.smithsonianmag.com/science-nature/Dear-Science-Fiction-Writers-Stop-Being-So-Pessimistic.html
| title         = Dear Science Fiction Writers: Stop Being So Pessimistic!
| publisher     = Smithsonianmag.com
| date          = April 2012
| access-date   = 2012-04-14
}}</ref>

<ref name="wpi_20110927_stephenson">{{cite web
| last          = Stephenson
| first         = Neal
| url           = http://www.worldpolicy.org/journal/fall2011/innovation-starvation
| title         = Innovation Starvation
| publisher     = Worldpolicy.org
| date          = 2011-09-27
| access-date   = 2012-04-14
}}</ref>

<ref name="asu_2012_hieroglyphhomepage">{{cite web
| author        = <!-- by ASU -->
| url           = http://hieroglyph.asu.edu/
| title         = Hieroglyph
| publisher     = Hieroglyph.asu.edu
| date          =
| access-date   = 2012-04-14
}}</ref>

<ref name="asu_2019_csihomepage">{{cite web
| author        = <!-- by ASU -->
| url           = http://csi.asu.edu/
| title         = Center for Science and the Imagination, Arizona State University
| access-date   = 2019-02-13
}}</ref>

<ref name="hieroglyph_2015_thebook">{{cite book
| title         = Hieroglyph : Stories and Visions for a Better Future
| editor-last1  = Finn
| editor-first1 = Ed
| editor-last2  = Cramer
| editor-first2 = Kathryn
| last1         = Stephenson
| first1        = Neal
| last2         = Goonan
| first2        = Kathleen Anne
| last3         = Ashby
| first3        = Madline
| last4         = Doctorow
| first4        = Cory
| last5         = Lee
| first5        = Konstantinou
| last6         = Schroeder
| first6        = Karl
| last7         = Newitz
| first7        = Annalee
| last8         = Landis
| first8        = Geoffrey
| last9         = Cambias
| first9        = James L.
| last10        = Benford
| first10       = Gregory
| last11        = Vandana
| first11       = Singh
| last12        = Cooper
| first12       = Brenda
| last13        = Bear
| first13       = Elizabeth
| last14        = Rucker
| first14       = Rudy
| last15        = Brin
| first15       = David
| last16        = Anders
| first16       = Charlie Jane
| last17        = Sterling
| first17       = Bruce
| date          = 2015
| publisher     = [[William Morrow (publisher)|William Morrow]]
| location      = New York
| oclc          = 904576842
| isbn          = 9780062204714
}}</ref>


}}

==Further reading==
*[http://www.chron.com/opinion/outlook/article/Innovation-stagnation-is-slowing-U-S-progress-2208324.php Innovation stagnation is slowing U.S. progress] by [[David Brooks (journalist)|David Brooks]], ''[[Houston Chronicle]]'', October 7, 2011.
*[https://www.bbc.com/news/magazine-28974943 Project Hieroglyph: Fighting society's dystopian future] by Debbie Siegelbaum, [[BBC News]], Washington, September 3, 2014.

[[Category:Adventure fiction]]
[[Category:Arizona State University]]
[[Category:Electronic literature]]
[[Category:Emerging technologies]]
[[Category:Futures studies]]
[[Category:Technology forecasting]]
[[Category:Technology in society]]
[[Category:Science fiction literature]]
[[Category:Technology development]]
[[Category:American science websites]]
