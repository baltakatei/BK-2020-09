{{Infobox Election
| election_name = [[Navajo Nation]] presidential election, 2018
| country = Navajo Nation
| flag_image = Navajo flag.svg
| type = presidential
| ongoing = no
| previous_election = Navajo Nation presidential election, 2015
| previous_year = 2015
| next_election = Navajo Nation presidential election, 2022
| next_year = 2022
| election_date = November 6, 2018

<!--    Jonathan Nez    -->| image1 = [[File:Jonathan Nez - 2020.jpg|x160px]]
| nominee1 = [[Jonathan Nez]]
| running_mate1 = [[Myron Lizer]]
| popular_vote1 = 39,783
| percentage1 = 66.38%

<!--    Joe Shirley Jr    -->| image2 = [[File:Joe Shirley.jpg|x160px]]
| nominee2 = [[Joe Shirley, Jr]]
| running_mate2 = [[Buu Nygren]]
| popular_vote2 = 20,146
| percentage2 = 33.62%
<!-- 
| map_image          = 
| map_size           = 
| map_caption        = County results 
-->| title = President
| before_election = [[Russell Begaye]]
| after_election = [[Jonathan Nez]]
| after_party = 
}}
{{ElectionsNN}}

The '''Navajo Nation presidential election of 2018''' was held on Tuesday, November 6, 2018.<ref name=NNEA_2018_calendar /> The candidates for [[President of the Navajo Nation]] in the general election were [[Jonathan Nez]] and [[Joe Shirley, Jr]].<ref name=dailytimes_2018-08-29_primaryresults /> Jonathan Nez and running mate Myron Lizer won the election.<ref name= "ABQJournal_2018-11-07_generalresults" />

The primary election was held on Tuesday, August 28, 2018 between 18 candidates for the office of Navajo Nation President.<ref name=NNEA_2018_primarycandidates />

==Results==
Jonathan Nez received 39,783 votes while Joe Shirley, Jr. received 20,146 votes.<ref name="NNEA_20181106_generalresults" />

A referendum for a salary increase<ref name="dailytimes_2018-09-11_salaryincrease" /> for President and Vice-President did not pass with 37,693 votes against and 18,802 votes for.<ref name="NNEA_20181106_generalresults" />

==References==
{{reflist|refs=

<ref name="NNEA_2018_calendar">{{cite web
| url          = http://www.navajoelections.navajo-nsn.gov/pdfs/2018/2018%20NN%20Election%20Timeline%20official_3.pdf
| title        = 2018 Navajo Election Calendar
| date         = 2017-04-21
| access-date  = 2018-10-13
| publisher    = Navajo Nation Election Administration
| archive-url  = https://web.archive.org/web/20181013202648/http://www.navajoelections.navajo-nsn.gov/pdfs/2018/2018%20NN%20Election%20Timeline%20official_3.pdf
| archive-date = 2018-10-13
| url-status     = live
}}</ref>

<ref name="NNEA_2018_primarycandidates">{{cite web
| url          = http://www.navajoelections.navajo-nsn.gov/pdfs/2018/2018%20NN%20Election%20Ballot%20Layout%20for%20the%20Public%208.15.18.pdf
| title        = Official 2018 Navajo Nation Election Candidates
| date         = 2018-08-15
| access-date  = 2018-10-13
| publisher    = Navajo Nation Election Administration
| archive-url  = https://web.archive.org/web/20181013211344/http://www.navajoelections.navajo-nsn.gov/pdfs/2018/2018%20NN%20Election%20Ballot%20Layout%20for%20the%20Public%208.15.18.pdf
| archive-date = 2018-10-13
| url-status     = live
}}</ref>

<ref name="dailytimes_2018-08-29_primaryresults">{{cite web
| url         = https://www.daily-times.com/story/news/politics/elections/2018/08/29/voters-back-nez-shirley-navajo-nation-presidential-race/1138608002/
| title        = Voters back Nez, Shirley in Navajo Nation presidential race
| date         = 2018-08-29
| access-date  = 2018-10-13
| work         = [[Farmington Daily Times]]
| archive-url  = https://web.archive.org/web/20180830105352/https://www.daily-times.com/story/news/politics/elections/2018/08/29/voters-back-nez-shirley-navajo-nation-presidential-race/1138608002/?from=new-cookie
| archive-date = 2018-08-30
| url-status     = live
}}</ref>

<ref name="dailytimes_2018-09-11_salaryincrease">{{cite news
| last          = Smith
| first         = Noel Lyn
| title         = Salary proposal for president, vice president sent to Begaye
| url           = https://www.daily-times.com/story/news/local/navajo-nation/2018/09/11/salary-proposal-president-vice-president-sent-begaye/1260194002/
| work          = [[Farmington Daily Times]]
| date          = 2018-09-11
| access-date   = 2018-11-09
| archive-url   = https://web.archive.org/web/20180911182759/https://www.daily-times.com/story/news/local/navajo-nation/2018/09/11/salary-proposal-president-vice-president-sent-begaye/1260194002/?from=new-cookie
| archive-date  = 2018-09-11
| url-status      = live
}}</ref>

<ref name="ABQJournal_2018-11-07_generalresults">{{cite news
| last          = Smith
| first         = Noel Lyn
| title         = Voters on the Navajo Nation elect president, council delegates
| url           = https://www.daily-times.com/story/news/politics/elections/2018/11/08/midterm-elections-2018-navajo-nation-new-mexico-results/1921939002/
| work          = [[Farmington Daily Times]]
| date          = 2018-11-08
| access-date   = 2018-11-09
| archive-url   = https://web.archive.org/web/20181108211639/https://www.daily-times.com/story/news/politics/elections/2018/11/08/midterm-elections-2018-navajo-nation-new-mexico-results/1921939002/?from=new-cookie
| archive-date  = 2018-11-08
| url-status      = live
}}</ref>

<ref name="NNEA_20181106_generalresults">{{cite web
| title         = 2018 Navajo Nation General Election Unofficial Results
| url           = http://www.navajoelections.navajo-nsn.gov/pdfs/2018/General%20results/2018NNGeneralUnofficialResults.pdf
| date          = 2018-11-06
| access-date   = 2018-11-09
| publisher     = Navajo Nation Election Administration
| archive-url   = https://web.archive.org/web/20181109130448/http://www.navajoelections.navajo-nsn.gov/pdfs/2018/General%20results/2018NNGeneralUnofficialResults.pdf
| archive-date  = 2018-11-09
| url-status      = live
}}</ref>

}}

==External links==
*[https://web.archive.org/web/20181013200822/https://www.nez-lizer.com/ Jonathan Nez and Myron Lizer campaign website]
*[https://www.joeshirleyjr.com/ Joe Shirley, Jr. and Buu Nygren campaign website]


[[Category:2018 elections in the United States]]
[[Category:Navajo Nation elections]]
[[Category:2018 Arizona elections]]
[[Category:2018 New Mexico elections]]
